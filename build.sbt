
val vicly_backend = project
  .in(file("."))
  .settings(
    libraryDependencies ++= Seq(
      ws,
      guice,
      "ru.tochkak" %% "play-plugins-salat" % "1.7.2",
      "com.pauldijou" %% "jwt-play-json" % "0.19.0",
      "org.typelevel" %% "cats-core" % "1.6.0",
      specs2 % Test,
      "org.mockito" % "mockito-core" % "2.18.0" % "test",
      "org.scalatest" %% "scalatest" % "3.0.5" % "test",
      "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.1" % "test"
    ),
    name := "backend",
    version := "0.1",
    scalaVersion := "2.12.8"
  )
  .enablePlugins(PlayScala)
